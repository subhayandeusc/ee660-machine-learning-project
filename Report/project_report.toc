\contentsline {section}{Project Homepage}{1}
\contentsline {section}{Dataset}{1}
\contentsline {section}{Problem Statements and Goals}{2}
\contentsline {section}{Literature Review}{2}
\contentsline {section}{Prior and Related Works}{2}
\contentsline {section}{Part 1: Regression problem}{2}
\contentsline {subsection}{Formulation and setup}{2}
\contentsline {subsubsection}{VC generalization bounds for regression problems}{3}
\contentsline {subsubsection}{Normalization}{3}
\contentsline {subsubsection}{Feature selection}{3}
\contentsline {subsubsection}{Feature reduction using Principal Component Analysis (PCA)}{4}
\contentsline {subsubsection}{Linear regression}{4}
\contentsline {subsubsection}{$\phi $-machine}{4}
\contentsline {subsubsection}{CART}{4}
\contentsline {subsubsection}{Support vector regression}{5}
\contentsline {subsubsection}{DNN}{6}
\contentsline {subsection}{Methodology}{6}
\contentsline {subsubsection}{Hypothesis set}{6}
\contentsline {subsubsection}{Training procedure}{7}
\contentsline {subsubsection}{Model selection and validation}{7}
\contentsline {subsubsection}{Testing}{7}
\contentsline {subsection}{Implementation}{7}
\contentsline {subsubsection}{Feature space}{7}
\contentsline {subsubsection}{Preprocessing}{7}
\contentsline {subsubsection}{Training, validation and model selection}{7}
\contentsline {subsection}{Final Results}{8}
\contentsline {subsection}{Interpretations}{9}
\contentsline {section}{Part 2: Classification problem}{9}
\contentsline {subsection}{Formulation and setup}{9}
\contentsline {subsubsection}{CART with PCA based feature reduction}{10}
\contentsline {subsubsection}{Support Vector Classification}{10}
\contentsline {subsection}{Methodology}{10}
\contentsline {subsection}{Implementation and Results}{10}
\contentsline {subsection}{Interpretations}{10}
\contentsline {section}{Part 3: Unsupervised learning}{10}
\contentsline {subsection}{Formulation and setup}{10}
\contentsline {subsubsection}{$k$-means clustering}{10}
\contentsline {subsubsection}{Gaussian mixture model (GMM)}{10}
\contentsline {subsubsection}{$k$-medoids}{10}
\contentsline {section}{Summary and Conclusions}{10}
\contentsline {section}{Individual Contribution List}{12}
