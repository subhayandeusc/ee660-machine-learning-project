%% Linear regression using augmented feature space -- Shao-Yen Tseng
clear all; clc ; 
% load YearPredRegData
% or
display('Loading data')
A=load('../YearPredictionMSD.txt');
Year=A(:,1);
mxy=max(Year);
mny=min(Year);
D=90;

yr=(Year-mny)./(mxy-mny);

% train/test data as suggested in UCI repository
Ntr=463715;
Ntest=numel(yr)-Ntr;
yrtrain=Year(1:Ntr);
yrtest=Year(Ntr+1:end);
ytrain=yr(1:Ntr);
ytest=yr(Ntr+1:end);

X=A(1:Ntr,2:end);
Xtest=A(Ntr+1:end,2:end);

% Augmented feature space
X_aug = [ones(Ntr,1), X];
Xtest_aug = [ones(Ntest,1), Xtest];

clear A

%% MLE weights for linear regression
display('Linear regression')
w_MLE = inv(X_aug'*X_aug)*X_aug'*ytrain;

Etrain=(ytrain-X_aug*w_MLE)'*(ytrain-X_aug*w_MLE)/Ntr
Etest=(ytest-Xtest_aug*w_MLE)'*(ytest-Xtest_aug*w_MLE)/Ntest




