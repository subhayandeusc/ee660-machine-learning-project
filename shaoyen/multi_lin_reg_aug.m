%% Multiclass classification - Lin. regr. in augmented feat. space -- Shao-Yen Tseng
clear all;
% load YearPredRegData
% or
A=load('../YearPredictionMSD.txt');
Year=A(:,1);
mxy=max(Year);
mny=min(Year);
D=90;

yr=(Year-mny)./(mxy-mny);

% train/test data as suggested in UCI repository
Ntr=463715;
Ntest=numel(yr)-Ntr;
yrtrain=Year(1:Ntr);
yrtest=Year(Ntr+1:end);
ytrain=yr(1:Ntr);
ytest=yr(Ntr+1:end);

X=A(1:Ntr,2:end);
Xtest=A(Ntr+1:end,2:end);

% Augmented feature space
X_aug = [ones(Ntr,1), X];
Xtest_aug = [ones(Ntest,1), Xtest];

% Group decades together
str_labels = ['oldies','70s','80s','90s','2ks'];
class_labels = zeros(numel(Year),1);
class_labels( Year < 1970 ) = 1;
class_labels( Year >= 1970 & Year < 1980) = 2;
class_labels( Year >= 1980 & Year < 1990) = 3;
class_labels( Year >= 1990 & Year < 2000) = 4;
class_labels( Year >= 2000) = 5;

lbltrain = class_labels(1:Ntr);
lbltest  = class_labels(Ntr+1:end);

%% MLE weights for linear regression
display('MLE classification')
w_MLE = inv(X_aug'*X_aug)*X_aug'*lbltrain;

acc_train=mean(lbltrain==round(X_aug*w_MLE)) * 100
acc_test=mean(lbltest==round(Xtest_aug*w_MLE)) * 100


%% Multi-class classification from regression values
w_MLE = inv(X_aug'*X_aug)*X_aug'*ytrain;

ypred = X_aug*w_MLE;
yrpred = (ypred * (mxy-mny) ) + mny ; 
pred_labels_train = zeros(numel(yrpred),1);
pred_labels_train( yrpred < 1970 ) = 1;
pred_labels_train( yrpred >= 1970 & yrpred < 1980) = 2;
pred_labels_train( yrpred >= 1980 & yrpred < 1990) = 3;
pred_labels_train( yrpred >= 1990 & yrpred < 2000) = 4;
pred_labels_train( yrpred >= 2000) = 5;

ypred = Xtest_aug*w_MLE;
yrpred = (ypred * (mxy-mny) ) + mny ; 
pred_labels_test = zeros(numel(yrpred),1);
pred_labels_test( yrpred < 1970 ) = 1;
pred_labels_test( yrpred >= 1970 & yrpred < 1980) = 2;
pred_labels_test( yrpred >= 1980 & yrpred < 1990) = 3;
pred_labels_test( yrpred >= 1990 & yrpred < 2000) = 4;
pred_labels_test( yrpred >= 2000) = 5;


acc_train_regr = mean( pred_labels_train == lbltrain) * 100
acc_test_regr  = mean( pred_labels_test  == lbltest) * 100
