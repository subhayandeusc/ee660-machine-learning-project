% ------------------------------------------------------------------
% A decision tree with binary splits for regression
% Author : Shao-Yen Tseng
% Date   : 11/10/15
% ------------------------------------------------------------------
clear all;
Ntr =463715;
Nts =51630;

mxyr = 2011;
mnyr = 1922;

if exist('Xtrain.mat','file') 
    load('Xtrain.mat')
    load('Xtest.mat')
    load('ytrain.mat')
    load('ytest.mat')
    load('yrtrain.mat')
    load('yrtest.mat')
else
    A=load('../YearPredictionMSD.txt');
    Year=A(:,1);
    mxy=max(Year);
    mny=min(Year);
    D=90;

    yr=(Year-mny)./(mxy-mny);
       
    ytrain=yr(1:Ntr);
    ytest=yr(Ntr+1:end);
    yrtrain = Year(1:Ntr);
    yrtest  = Year(Ntr+1:end);

    Xtrain=A(1:Ntr,2:end);
    Xtest=A(Ntr+1:end,2:end);
    clear A
end

%% Train the regression tree

tree = fitrtree(Xtrain,ytrain);


%% Evaluate on Test

ypred = tree.predict(Xtest);
mse = mean((ypred - ytest).^2)

%% Plot prediction 

z = sort([ytest,ypred],1)*(mxyr-mnyr) + mnyr;
figure(1)
hold off
plot(z(:,1))
hold on
plot(z(:,2))
xlabel('Sample')
ylabel('Year')
legend('Test','Predicted')

