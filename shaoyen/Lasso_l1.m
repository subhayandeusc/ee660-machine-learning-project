%% Feature reduction using l1 regularization -- Shao-Yen Tseng
clear all;
% load YearPredRegData
% or
A=load('../YearPredictionMSD.txt');
Year=A(:,1);
mxy=max(Year);
mny=min(Year);
D=90;

yr=(Year-mny)./(mxy-mny);

% train/test data as suggested in UCI repository
Ntr=463715;
Ntest=numel(yr)-Ntr;
yrtrain=Year(1:Ntr);
yrtest=Year(Ntr+1:end);
ytrain=yr(1:Ntr);
ytest=yr(Ntr+1:end);

X=A(1:Ntr,2:end);
Xtest=A(Ntr+1:end,2:end);

% Augmented feature space
X_aug = [ones(Ntr,1), X];
Xtest_aug = [ones(Ntest,1), Xtest];

clear A

%% l1 regularization
numlambda = 200 ;
lambda =  logspace(-6,6,numlambda) ;

% Lasso fit with a maximum of i non-zero coefficients
[B, fitinfo] = lasso(X_aug, ytrain, 'DFMax', 50, 'CV', 3 , 'Lambda', lambda);   
     
 

%% Final Performance
X_red_tr = X_aug(:, B(:,fitinfo.IndexMinMSE) ~= 0);
X_red_ts = Xtest_aug(:, B(:,fitinfo.IndexMinMSE) ~= 0);

final_W = inv(X_red_tr'*X_red_tr)*X_red_tr'*ytrain;

mse_tr  = mean(( ytrain - (X_red_tr*final_W)).^2) 
mse_ts  = mean(( ytest - (X_red_ts*final_W)).^2) 







