#!/bin/bash

if [ -z "$1" ] ; then
    echo "Missing argument"
    exit 1
fi

batch_file=$1

for run in `cat $batch_file` ; do
    echo $run
    if [ ! -d $run ] ; then
        mkdir $run 
        mkdir $run/checkpoint
        mkdir $run/log
    fi
    cd $run
    if [ ! -f $run.json ] ; then 
        ln -s ../config/$run.json
    fi
    (python -u ../DNN_test.py $run.json 2>&1| tee $run_test.log || exit 1) &
    cd ..
done

wait

