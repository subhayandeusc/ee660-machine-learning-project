#!/bin/python
# ==========================================================================================
# Author        : Shao-Yen Tseng
# Date          : 11-24-15
# Description   : Regression using DNN for years of MSD dataset (test)
# ==========================================================================================

import os , sys
import json
import tensorflow as tf
import numpy as np
import scipy.io

def iterate_minibatches(inputs, targets, batchsize, shuffle=True):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]


P_DATASET = '../YearPredictionMSD.txt'

P_RESTORE = False # True

# Parameters
P_SEED      = 5566
P_EPOCHS    = 10

P_N_TR = 463715
P_N_TS = 51630

if len(sys.argv) == 1 :
    config = { 'dimensions': [90,90,90,5], 'activations': ['tanh','relu','relu','softmax'], 'dropout': [ False, False, False, False]}
else :
    config = json.load(open(sys.argv[1]))


P_DIM_HID   = config['dimensions']
P_ACTIV_HID = config['activations']
P_DROP_HID  = config['dropout']
P_DIM_IN    = 90
P_DIM_OUT   = P_DIM_HID[-1]

# Load data
print "Loading data..."
X_train = scipy.io.loadmat('MSD.mat')['Xtrain']
y_train = scipy.io.loadmat('MSD.mat')['lbltrain_onehot']
X_test  = scipy.io.loadmat('MSD.mat')['Xtest']
y_test  = scipy.io.loadmat('MSD.mat')['lbltest_onehot']

# Build NN
print "Building NN..."
x  = tf.placeholder("float" , shape=[None, P_DIM_IN], name = 'input')
y_ = tf.placeholder("float", shape=[None, P_DIM_OUT], name = 'target')
keep_prob = tf.placeholder("float", name = 'keep_prob')

lay_weights_bias = []
network = x
for i in range(len(P_DIM_HID)) :
    if i == 0 :
        lay_in_dim = P_DIM_IN 
    else :
        lay_in_dim = P_DIM_HID[i-1] 

    lay_out_dim = P_DIM_HID[i]

    with tf.name_scope('layer_{}'.format(i)) as scope:
        weights = tf.Variable(tf.random_uniform([lay_in_dim,lay_out_dim], seed=P_SEED))
        bias    = tf.Variable(tf.random_uniform([lay_out_dim], seed=P_SEED))

        lay_weights_bias.append(weights)
        lay_weights_bias.append(bias)
    
        network = tf.matmul(network, lay_weights_bias[2*i],name='Mul{}'.format(i)) 
        network = tf.add(network, lay_weights_bias[2*i+1], name='Add{}'.format(i))
        
        if 'linear' in P_ACTIV_HID[i] :
            pass
        elif 'tanh' in P_ACTIV_HID[i] :
            network = tf.tanh(network)
        elif 'sigmoid' in P_ACTIV_HID[i] :
            network = tf.sigmoid(network)
        elif 'relu' in P_ACTIV_HID[i] :
            network = tf.nn.relu(network)
        elif 'softmax' in P_ACTIV_HID[i] :
            network = tf.nn.softmax(network)
        else :
            raise ValueError('Hidden layer activation type unknown')
    
    if P_DROP_HID[i] :
        network = tf.nn.dropout(network, keep_prob)

y = network

# Define loss function
cross_entropy = -tf.reduce_sum(y_*tf.log(y),name='loss')
loss = cross_entropy

global_step = tf.Variable(0, trainable=False)
#S starter_learning_rate = 0.005
#S learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step,
#S                                                        100000, 0.96, staircase=True)
#S train_opt = tf.train.GradientDescentOptimizer(learning_rate)

# train_opt = tf.train.AdamOptimizer(1e-4)

train_opt = tf.train.AdagradOptimizer(0.1)

grads_vars = train_opt.compute_gradients(loss)
#S capped_grads = [(tf.clip_by_value(grad, P_GRAD_MIN, P_GRAD_MAX), var) for grad,var in grads_vars]
#S capped_grads = [(tf.clip_by_norm(grad, P_GRAD_MAX), var) for grad,var in grads_vars]

#S train_step = train_opt.apply_gradients(capped_grads)
train_step = train_opt.minimize(loss, global_step=global_step)

y_class_pred = tf.argmax(y,1)
correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

#tf.scalar_summary('Cross Entropy', cross_entropy)

# Create a saver.
saver = tf.train.Saver(lay_weights_bias)

sess = tf.InteractiveSession()

ckpt = tf.train.get_checkpoint_state(checkpoint_dir=os.getcwd()+'/checkpoint_multiclass')
if ckpt and ckpt.model_checkpoint_path :
    print "Restore model from",ckpt.model_checkpoint_path
    saver.restore(sess, ckpt.model_checkpoint_path)
else :
    print "***ERROR: Checkpoint file not found"
    print os.getcwd()+'/checkpoint_multiclass'
    sys.exit()

# ===================================================
# Test sequence
# ===================================================
print "Running test sequence..."

#S y_pred = y.eval(feed_dict = { x: X_test, keep_prob: 1.0})
[y_ts_pred , ts_mse] = sess.run([y_class_pred, accuracy],feed_dict = {x: X_test,  y_ : y_test,    keep_prob :1.0})
[y_tr_pred , tr_mse] = sess.run([y_class_pred, accuracy],feed_dict = {x: X_train, y_ : y_train, keep_prob :1.0})

scipy.io.savemat('DNN_y_pred.mat', {'y_ts_pred_dnn': y_ts_pred, 'y_tr_pred_dnn': y_tr_pred})

print "Final accuracy on train:", tr_mse
print "Final accuracy on test:", ts_mse


sess.close()

