# README #

This repository is created for EE660: Machine Learning from Signals: Foundations and Methods' project at University of Southern California.

### What is this repository for? ###

* Quick summary

Two folders named Subhayan and Shao-yen contains codes written for the musical year prediction database uploaded at UCI machine learning repository (*https://archive.ics.uci.edu/ml/datasets/YearPredictionMSD*). Different machine learning algorithms are tried and shown to give improved performance.
Required packages:

      (1) PMTK

      (2) LIBSVM

      (3) MATLAB (with Statistics toolbox)

      (4) PYTHON (numpy,scipy,tensorflow)


### How do I get set up? ###

* Install MATLAB, PMTK, LIBSVM.

* Download *YearPredictionMSD.txt* from the repository.

* Run *preprocess_regression.m* and *preprocess_classifcation.m* in the *Subhayan* folder for preprocessing of the text file for the regression and multiclass classification problems.

* The *Subhayan* folder contains the following codes

      (1) *project_baseline.m* performs linear regression with all or reduced feature set;

      (2) *basisfunexpan.m* implements phi-machine;

      (3) *svr_libsvm.m* performs support vector regression; (requires LIBSVM)

      (4) *CART_multiclass_classification.m* performs multiclass classification using CART; (requires PMTK)

      (5) *svm_classification.m* implements support vector classification; (requires LIBSVM)

      (6) *SBR_l0reg.m*, *OMP_l0reg.m*, *SBR_l0regCV.m*, and *OMP_l0regCV.m* implement two algorithms for l_0 regularized feature selection with training MSE and cros--validation errors, respectively;
      
      (7) *k_medoids.m* implements k-medoids clustering algorithm for unsupervised learning with the dataset;
      
      (8) *gmm_USL.m* implements Gaussian Mixture Model (GMM) clustering algorithm for unsupervised learning with the dataset;
      
      (9) *k_means.m* implements K-means clustering algorithm for unsupervised learning with the dataset.

* The *shaoyen* folder contains the following codes
      
      (1) *lin_reg_aug.m* performs linear regression with augmented feature space;

      (2) *mult_lin_reg_aug.m* performs classification using linear regression with augmented feature space;

      (3) *Decision_Trees.m* performs regression using binary trees;

      (4) *Lasso_l1.m* performs feature reduction using l1 regularization;

      (5) The DNN folder performs regression and classification using neural networks;

* For details about the implementation of these methods please see the *project_report.pdf* inside the folder named *Report*.


### Who do I talk to? ###

* Subhayan De (subhayad@usc.edu), Shao-Yen Tseng (shaoyent@usc.edu)

Best,

Subhayan, Shao-Yen

--