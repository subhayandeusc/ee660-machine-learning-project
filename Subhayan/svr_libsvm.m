%% SVR Support Vector Regression using LIBSVM -- Subhayan De 11/29/2015
% addpath(genpath('Users/subhayan_apple/Documents/MATLAB/Courseworks/EE660/Project/machine-learning-project/libsvm-3-2.20/matlab'))

load YearPredRegData.mat
[X, mu, s] = standardizeCols(X);
[Xtest, mutest, stest]=standardizeCols(Xtest);
rng(1);
% simple cross-validation
elem=datasample(1:Ntr,round(0.01*Ntr),'Replace',false);
training_instance_matrix=X(elem,:);
training_label_vector=yrtrain(elem);
ntr=round(0.9*numel(elem));
nval=numel(elem)-ntr+1;
ytr=training_label_vector(1:ntr);
xtr=training_instance_matrix(1:ntr,:);
yts=training_label_vector(ntr+1:end);
xts=training_instance_matrix(ntr+1:end,:);

c=[2^-3 2^-2 2^-1 1 2 2^2 2^3 2^4 2^5 2^6];
% c=2^5;
% gamma=2^(-3);
gamma=[2^-3 2^-2 2^-1 1 2 2^2 2^3];
nu=0.1:0.1:0.9;

for i=1:numel(c)
    for j=1:numel(gamma)
        for k=1:numel(nu)
        i
        j
        k
        libsvm_options=['-s 4 -t 2 -g ' num2str(gamma(j)) ' -c ' num2str(c(i)) ' -n ' num2str(nu(k))];
%         libsvm_options=['-s 4 -t 2 -c ' num2str(c(i)) ' -h 0'];
        ts=cputime;
%         model = svmtrain(ytr,xtr ,libsvm_options);
        model = svmtrain(training_label_vector,training_instance_matrix ,libsvm_options);
%         [predicted_label, accuracy, prob_estimates] = svmpredict(yts, xts, model);
        [predicted_label, accuracy, prob_estimates] = svmpredict(yrtest, Xtest, model);
        err(i)=accuracy(2);
        tf=cputime-ts
        end
    end
end




% ts=cputime;
% model = svmtrain(training_label_vector, training_instance_matrix,libsvm_options);

% [predicted_label, accuracy, prob_estimates] = svmpredict(yrtest, Xtest, model);
% tf=cputime-ts