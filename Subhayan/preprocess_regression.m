%% Pre-processing of the data file YearPredictionMSD.txt - Subhayan De
% run before running anything else on regression
A=load('YearPredictionMSD.txt');
Year=A(:,1);
mxy=max(Year);
mny=min(Year);
D=90;

yr=(Year-mny)./(mxy-mny);

% train/test data as suggested in UCI repository
Ntr=463715;
Ntest=numel(yr)-Ntr;
yrtrain=yr(1:Ntr);
yrtest=yr(Ntr+1:end);

X=A(1:Ntr,2:end);
Xtest=A(Ntr+1:end,2:end);
clear A
save YearPredRegData.mat