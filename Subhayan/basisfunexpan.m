%% Basis function expansion -- Subhayan De 11/18/2015
clear all
% load feat_red_40
load YearPredRegData.mat
% A=load('YearPredictionMSD.txt');
% Year=A(:,1);
% mxy=max(Year);
% mny=min(Year);
% D=90;
% 
% yr=(Year-mny)./(mxy-mny);
% 
% % train/test data as suggested in UCI repository
% Ntr=463715;
% Ntest=numel(yr)-Ntr;
% yrtrain=yr(1:Ntr);
% yrtest=yr(Ntr+1:end);
% 
% X=A(1:Ntr,2:end);
% Xtest=A(Ntr+1:end,2:end);
% clear A

% % normalize features
% X=standardizeCols(X);
% Xtest=standardizeCols(Xtest);

% % select first 12 features
% red_feat=1:12;
% X_red=X(:,red_feat);
% X_redtest=Xtest(:,red_feat);

% % PCA based reduction (!Important note: PCA of MATLAB NOT of PMTK)
% [coeff,score,latent] = pca(X);
% [coeff2,score2,latent2] = pca(Xtest);
% 
% Xred=score(:,1:30);
% Xredtest=score2(:,1:30);
% 
% % Basis function expansion
% phi=[ones(numel(yrtrain),1) Xred Xred.^2];
% phi_test=[ones(numel(yrtest),1) Xredtest Xredtest.^2];

phi=[ones(numel(yrtrain),1) X X.^2];
phi_test=[ones(numel(yrtest),1) Xtest Xtest.^2];

% % with only reduced features
% w_MLE=(X_red'*X_red)\X_red'*yrtrain;
% % mean-squared error in training and test data
% Etest=(yrtest-X_redtest*w_MLE)'*(yrtest-X_redtest*w_MLE)/Ntest
% Etrain=(yrtrain-X_red*w_MLE)'*(yrtrain-X_red*w_MLE)/Ntr

% with basis function expansion
w_MLEphi=(phi'*phi)\phi'*yrtrain;
Etest=(yrtest-phi_test*w_MLEphi)'*(yrtest-phi_test*w_MLEphi)/Ntr
Etrain=(yrtrain-phi*w_MLEphi)'*(yrtrain-phi*w_MLEphi)/Ntr

%% Errors in terms of years
% test data
Yhattst=round(mny+(mxy-mny)*(phi_test*w_MLEphi));
Etst=(Year(Ntr+1:end)-Yhattst)'*(Year(Ntr+1:end)-Yhattst)/Ntest;
sqrt(Etst)
Eabstst=mean(abs((Year(Ntr+1:end)-Yhattst)))

% %training data
% Yhattr=round(mny+(mxy-mny)*(phi*w_MLEphi));
% Etr=(Year(1:Ntr)-Yhattr)'*(Year(1:Ntr)-Yhattr)/Ntr;
% sqrt(Etr);
% Eabstr=mean(abs((Year(1:Ntr)-Yhattr)));
% 
% % Check: for how many songs the prediction year is exact
% % test data
% wrong_pred=sum(Yhattst-Year(Ntr+1:end)~=0)/Ntest*100
% cor_pred=100-wrong_pred
% % training data
% wrong_pred=sum(Yhattr-Year(1:Ntr)~=0)/Ntr*100
% cor_pred=100-wrong_pred

% %% Errors in terms of years
% % test data
% Yhattst=round(mny+(mxy-mny)*(X_redtest*w_MLE));
% Etst=(Year(Ntr+1:end)-Yhattst)'*(Year(Ntr+1:end)-Yhattst)/Ntest;
% sqrt(Etst)
% Eabstst=mean(abs((Year(Ntr+1:end)-Yhattst)))
% 
% %training data
% Yhattr=round(mny+(mxy-mny)*(X_red*w_MLE));
% Etr=(Year(1:Ntr)-Yhattr)'*(Year(1:Ntr)-Yhattr)/Ntr;
% sqrt(Etr)
% Eabstr=mean(abs((Year(1:Ntr)-Yhattr)))
% 
% Check: for how many songs the prediction year is exact
% test data
wrong_pred=sum(Yhattst-Year(Ntr+1:end)~=0)/Ntest*100
cor_pred=100-wrong_pred
% % training data
% wrong_pred=sum(Yhattr-Year(1:Ntr)~=0)/Ntr*100
% cor_pred=100-wrong_pred