%% Feature reduction using l0 regularization (SBR algorithm) -- Subhayan De
clear all;
load YearPredRegData
[X, mu, s] = standardizeCols(X);
[Xtest, mutest, stest]=standardizeCols(Xtest);

%% Single Best Replacement (SBR) with Orthogonal least squares/Greedy forwards selection (Murphy page 428)

gammat=[];
gammac=1:D;
for k=1:40
    k
    % step i
    for i=1:numel(gammac)
        gammat1=[gammat gammac(i)];
        Xtr=[ones(numel(yrtrain),1) X(:,gammat1)];
        w_MLE=(Xtr'*Xtr)\Xtr'*yrtrain;
        Etrain(i)=(yrtrain-Xtr*w_MLE)'*(yrtrain-Xtr*w_MLE)/Ntr;
    end
    [miner(k),imin]=min(Etrain);
    gammat=[gammat gammac(imin)];
    clear gammac
    gammac=[];
    for i=1:D
        counter=1;
        for j=1:numel(gammat)
            if i==gammat(j)
                counter=0;
            end
        end
        if counter~=0
            gammac=[gammac i];
        end
    end
end

% save feat_red_40 miner gammat gammac
%% Plot the change in training error with increasing number of features
% load feat_red_40
% MSE for training data using all 90 features
w_MLE=(X'*X)\X'*yrtrain;
Etrain=(yrtrain-X*w_MLE)'*(yrtrain-X*w_MLE)/Ntr;
plot(Etrain*ones(numel(miner),1),'color','b','LineWidth',2);
hold on;
plot(miner,'-o','color','r','LineWidth',1.5,'MarkerSize',8)
xlim([1 40]);
ylim([0.0115 0.0240]);
xlabel('Number of features ($D$)','interpreter','latex');
ylabel('Mean squared error (MSE)','interpreter','latex');
title('Feature reduction using $l_0$ regularization','interpreter','latex');
ax1 = gca;
hl1=legend({'all $D=90$ features','reduced features using SBR'},'interpreter','latex','fontsize',20);
set(hl1,'color',[0.89 .94 .9]);
ax1.LineWidth=1.0;
ax1.FontName='Times';
ax1.FontSize=20;