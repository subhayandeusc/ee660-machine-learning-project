%% SVM for multiclass classification by Subhayan De 12/04/2015
% requires LIBSVM
load class_label;
load YearPredRegData.mat

% clslbl=cellstr(cls_label);
lbl=unique(clslbl);
for i=1:5
    for j=1:numel(clslbl)
        if (isequal(clslbl{j},lbl{i}))
            ylbl(j)=i;
        end
        
    end
end
yrtest=ylbl(Ntr+1:end)';
rng(1);
elem=datasample(1:Ntr,round(0.02*Ntr),'Replace',false);
training_instance_matrix=X(elem,:);
training_label_vector=ylbl(elem)';

% c=[2^-3 2^-2 2^-1 1 2 2^2 2^3 2^4 2^5 2^6];
c=2^5;
gamma=2^(-3);
% gamma=[2^-3 2^-2 2^-1 1 2 2^2 2^3];
for i=1:numel(c)
    for j=1:numel(gamma)
        i
        j
        libsvm_options=['-s 0 -t 2 -g ' num2str(gamma(j)) ' -c  ' num2str(c(i))];
%         libsvm_options=['-s 4 -t 2 -c ' num2str(c(i)) ' -h 0'];
%         ts=cputime;
%         model = svmtrain(ytr,xtr ,libsvm_options);
        model = svmtrain(training_label_vector,training_instance_matrix ,libsvm_options);
%         [predicted_label, accuracy, prob_estimates] = svmpredict(yts, xts, model);
        [predicted_label, accuracy, prob_estimates] = svmpredict(yrtest, Xtest, model);
%         err(i,j)=accuracy(2);
    end
end