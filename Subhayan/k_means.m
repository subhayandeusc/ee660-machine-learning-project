%% k-means -- by Subhayan De
% require Statistics Toolbox of MATLAB
clear all
rng(1);
load YearPredRegData.mat

% % normalize features
X=standardizeCols(X);
Xtest=standardizeCols(Xtest);

Xtr=X(1:round(0.05*Ntr),:);

% [coeff,score,latent] = pca(X);

% Xtr=score(1:round(0.05*Ntr),1:30);

eva = evalclusters(Xtr,'kmeans','DaviesBouldin','KList',1:10);
Kopt=eva.OptimalK;

% Xtr=X(1:round(0.05*Ntr),:);
%%
rng(1)
[idx, C, sumd]=kmeans(Xtr,Kopt,'Distance','cityblock');
figure(1)
Ytr=zeros(Kopt,round(0.05*Ntr));
ptsymb = {'bs','r^','md','go','c+','ys', 'm^', 'kd', 'gd'};

for i = 1:Kopt
clust = find(idx==i);
Ncl(i)=numel(clust);
Ytr(i,1:Ncl(i))=Year(clust);
plot3(Xtr(clust,1),Xtr(clust,2),Xtr(clust,3),ptsymb{i});
hold on;
end

% figure(2)
% [s,h]=silhouette(Xtr,idx);
% title('Silhouette Plot of musical year prediction: K-means');

%% Analyze the song years in each cluster
% figure(3)
% plot(Ytr(1,:),'*')
% hold on;
% plot(Ytr(2,:),'o')
% ylim([1922 2011]);

%% Prepare the plot
hl1=legend({'Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5','Cluster 6','Cluster 7','Cluster 8','Cluster 9'},'interpreter','latex','fontsize',20);
set(hl1,'color',[0.98 .97 .97]);
xlabel('$x_1$','interpreter','latex','fontsize',24);
ylabel('$x_2$','interpreter','latex','fontsize',24);
zlabel('$x_3$','interpreter','latex','fontsize',24);
% xlim([-6 6])
% ylim([-6 6])
% zlim([-6 6])
title('$K$-means clustering','interpreter','latex','fontsize',20);
orient landscape
% saveas(gcf,'k_means_cityblock.png');
