% Baseline studies -- Subhayan De
addpath(genpath('Users/subhayan_apple/Documents/MATLAB/Courseworks/EE660/pmtk3-master'))
clear all
load YearPredRegData.mat


% % normalize features
X=standardizeCols(X);
Xtest=standardizeCols(Xtest);


% % augment the feature space
% X=[ones(numel(yrtrain),1) X];
% Xtest=[ones(numel(yrtest),1) Xtest];

%% MLE weights for linear regression
% w_MLE=(X'*X)\X'*yrtrain;
% % mean-squared error in training and test data
% Etest=(yrtest-Xtest*w_MLE)'*(yrtest-Xtest*w_MLE)/Ntest;
% Etrain=(yrtrain-X*w_MLE)'*(yrtrain-X*w_MLE)/Ntr;

% % MAP 
% lambda=1e2;
% w_MAP=(X'*X+lambda*eye(D+1))\X'*yrtrain;
% Etest=(yrtest-Xtest*w_MAP)'*(yrtest-Xtest*w_MAP)/Ntest
% Etrain=(yrtrain-X*w_MAP)'*(yrtrain-X*w_MAP)/Ntr

%% With reduced features (12 timbre means)

% PCA based reduction (!Important note: PCA of MATLAB NOT of PMTK)
[coeff,score,latent] = pca(X);
[coeff2,score2,latent2] = pca(Xtest);

Xred=score(:,1:30);
Xredtest=score2(:,1:30);

% augment the feature space
Xred=[ones(numel(yrtrain),1) Xred];
Xredtest=[ones(numel(yrtest),1) Xredtest];
% 

w_MLE=(Xred'*Xred)\Xred'*yrtrain;
Etest=(yrtest-Xredtest*w_MLE)'*(yrtest-Xredtest*w_MLE)/Ntest
Etrain=(yrtrain-Xred*w_MLE)'*(yrtrain-Xred*w_MLE)/Ntr

% lambda=1;
% w_MAP=(Xred'*Xred+lambda*eye(12))\Xred'*yrtrain;
% Etest=(yrtest-Xredtest*w_MAP)'*(yrtest-Xredtest*w_MAP)/Ntest
% Etrain=(yrtrain-Xred*w_MAP)'*(yrtrain-Xred*w_MAP)/Ntr

%% Errors in terms of years
% test data
Yhattst=round(mny+(mxy-mny)*(Xredtest*w_MLE));
% Etst=(Year(Ntr+1:end)-Yhattst)'*(Year(Ntr+1:end)-Yhattst)/Ntest;
% sqrt(Etst)
% Eabstst=mean(abs((Year(Ntr+1:end)-Yhattst)))
% 
% %training data
% Yhattr=round(mny+(mxy-mny)*(X*w_MLE));
% Etr=(Year(1:Ntr)-Yhattr)'*(Year(1:Ntr)-Yhattr)/Ntr;
% sqrt(Etr)
% Eabstr=mean(abs((Year(1:Ntr)-Yhattr)))
% 
% % Check: for how many songs the prediction year is exact
% % test data
wrong_pred=sum(Yhattst-Year(Ntr+1:end)~=0)/Ntest*100
cor_pred=100-wrong_pred
% % training data
% wrong_pred=sum(Yhattr-Year(1:Ntr)~=0)/Ntr*100
% cor_pred=100-wrong_pred