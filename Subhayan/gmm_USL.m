%% Gaussian Mixture Model with PCA -- by Subhayan De
% require Statistics Toolbox of MATLAB
clear all
rng(1);
load YearPredRegData.mat

% % normalize features
X=standardizeCols(X);
Xtest=standardizeCols(Xtest);

% PCA based reduction
% [coeff,score,latent] = pca(X);
% Xtr=score(1:round(0.05*Ntr),1:30);

Xtr=X(1:round(0.05*Ntr),:);
%%
rng(1);
eva = evalclusters(Xtr,'gmdistribution' ,'DaviesBouldin','KList',1:10);
ptsymb = {'bs','r^','go','md','c+','ys', 'm^', 'kd'};
Kopt=eva.OptimalK;
rng(1);
gm = fitgmdist(Xtr,Kopt);
id = cluster(gm,Xtr);
figure(3)
for i = 1:Kopt
clust = find(id==i);
plot3(Xtr(clust,1),Xtr(clust,2),Xtr(clust,3),ptsymb{i});
hold on
end

%% Prepare the plot
hl1=legend({'Cluster 1','Cluster 2','Cluster 3'},'interpreter','latex','fontsize',20);
set(hl1,'color',[0.98 .97 .97]);
xlabel('$x_1$','interpreter','latex','fontsize',24);
ylabel('$x_2$','interpreter','latex','fontsize',24);
zlabel('$x_3$','interpreter','latex','fontsize',24);
% xlim([-6 6])
% ylim([-6 6])
% zlim([-6 6])
title('GMM clustering','interpreter','latex','fontsize',20);
orient landscape
saveas(gcf,'gmm.png');
