%% Pre-processing of the data file for multiclass classification - Subhayan De
% run before running anything else on classification
A=load('YearPredictionMSD.txt');
Year=A(:,1);
mxy=max(Year);
mny=min(Year);
D=90;

yr=(Year-mny)./(mxy-mny);

% train/test data as suggested in UCI repository
Ntr=463715;
Ntest=numel(yr)-Ntr;
yrtrain=yr(1:Ntr);
yrtest=yr(Ntr+1:end);

X=A(1:Ntr,2:end);
Xtest=A(Ntr+1:end,2:end);
clear A
% save YearPredRegData.mat
% load YearPredRegData.mat
cls_label=[];
for i=1:numel(Year)
    if Year(i)<1970
        cls='oldies';
    elseif Year(i)<1980
        cls='70s   ';
    elseif Year(i)<1990
        cls='80s   ';
    elseif Year(i)<2000
        cls='90s   ';
    else
        cls='2000s ';
    end
    cls_label=[cls_label;cls];
end
% load class_label;
clslbl=cellstr(cls_label);
save class_label.mat clslbl;