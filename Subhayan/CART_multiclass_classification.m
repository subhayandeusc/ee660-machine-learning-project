%% CART by Subhayan De
% requireStatsToolbox
% 
% "Resubstitution error" is the training set error
% load fisheriris
% N = size(meas,1);
load YearPredRegData.mat
% cls_label=[];
% for i=1:numel(Year)
%     if Year(i)<1970
%         cls='oldies';
%     elseif Year(i)<1980
%         cls='70s   ';
%     elseif Year(i)<1990
%         cls='80s   ';
%     elseif Year(i)<2000
%         cls='90s   ';
%     else
%         cls='2000s ';
%     end
%     cls_label=[cls_label;cls];
% end
% load class_label;
% clslbl=cellstr(cls_label);
load class_label
rng(1);
elem=datasample(1:Ntr,round(0.02*Ntr),'Replace',false);
training_instance_matrix=X(elem,:);
training_label_vector=clslbl(elem,:);

[coeff,score,latent] = pca(training_instance_matrix);
[coeff2,score2,latent2] = pca(Xtest);


Xred=score(:,1:15);

% require pmtk from this point
% fit tree
t = classregtree(Xred, training_label_vector(:,:));



yy=eval(t,score2(:,1:15));
yy2=clslbl(Ntr+1:end,:);
sum=0;
for i=1:Ntest
    if isequal(yy(i),yy2(i))
        sum=sum+1;
    end
end

% Percentage accuracy
accuracy=sum/Ntest*100
    
    
tp=prune(t,'level',35);
% plot tree
% view(tp)
