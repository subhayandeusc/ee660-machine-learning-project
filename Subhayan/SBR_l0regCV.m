%% Feature reduction using l0 regularization (SBR algorithm) -- Subhayan De
clear all;
load YearPredRegData
[X, mu, s] = standardizeCols(X);
[Xtest, mutest, stest]=standardizeCols(Xtest);

%% Single Best Replacement (SBR) with Orthogonal least squares/Greedy forwards selection (Murphy page 428)

gammat=[];
gammac=1:D;
% 5-fold cross-validation set
Indices = crossvalind('Kfold', Ntr, 5);
NfoldX=zeros(ceil(Ntr/5),5,D);
Nfoldy=zeros(ceil(Ntr/5),5);

for i=1:5
    ind_dvd=find(Indices==i);
    NfoldX(:,i,:)=X(ind_dvd,:);
    Nfoldy(:,i)=yrtrain(ind_dvd);
end


for k=1:40
    k
    % step i
    for i=1:numel(gammac)
        i
    % cross-validate
    EtrainCV=zeros(5,1);
    for ii=1:5
%         ii
        X_train=[];y_train=[];
        for jj=1:5
            if jj~=ii
                X_train=[X_train;reshape(NfoldX(:,jj,:),[ceil(Ntr/5),D])];
                y_train=[y_train;reshape(Nfoldy(:,jj),[ceil(Ntr/5),1])];
            else
                X_val=reshape(NfoldX(:,jj,:),[ceil(Ntr/5),D]);
                y_val=reshape(Nfoldy(:,jj),[ceil(Ntr/5),1]);
            end
        end
        Ntrain=numel(y_train);
        % augment the feature space
        X_train=[ones(numel(y_train),1) X_train];
        X_val=[ones(numel(y_val),1) X_val];
%             end
        
    

        gammat1=[gammat gammac(i)];
        w_MLE=(X_train(:,gammat1)'*X_train(:,gammat1))\X_train(:,gammat1)'*y_train;
        EtrainCV(ii)=(y_val-X_val(:,gammat1)*w_MLE)'*(y_val-X_val(:,gammat1)*w_MLE)/(Ntr-Ntrain+1);
    end
    Etrain(i)=mean(EtrainCV)
    end
    [miner(k),imin]=min(Etrain);
    gammat=[gammat gammac(imin)];
    clear gammac Etrain
    gammac=[];
    for i=1:D
        counter=1;
        for j=1:numel(gammat)
            if i==gammat(j)
                counter=0;
            end
        end
        if counter~=0
            gammac=[gammac i];
        end
%     end
    end
end

% save feat_red_40 miner gammat gammac
%% Plot the change in training error with increasing number of features
% load feat_red_40
load feat_red_aug40_std_SBR
% MSE for training data using all 90 features
X1=[ones(numel(yrtrain),1) X];
w_MLE=(X1'*X1)\X1'*yrtrain;
Etrain=(yrtrain-X1*w_MLE)'*(yrtrain-X1*w_MLE)/Ntr;
plot(Etrain*ones(numel(miner),1),'color','b','LineWidth',2);
hold on;
plot(miner,'-o','color','r','LineWidth',1.5,'MarkerSize',8)
xlim([1 40]);
ylim([0.0110 0.0180]);
xlabel('Number of features ($D$)','interpreter','latex');
ylabel('Cross-validation error','interpreter','latex');
title('Feature reduction using $l_0$ regularization','interpreter','latex');
ax1 = gca;
hl1=legend({'all $D=90$ features','reduced features using SBR'},'interpreter','latex','fontsize',20);
set(hl1,'color',[0.89 .94 .9]);
ax1.LineWidth=1.0;
ax1.FontName='Times';
ax1.FontSize=20;

%% Result of MLE using first N features from the SBR result
% load YearPredRegData
% load feat_red_aug40_std_SBR
% [X, mu, s] = standardizeCols(X);
% [Xtest, mutest, stest]=standardizeCols(Xtest);
% Xred=[ones(numel(yrtrain),1) X(:,gammat(1:30))];
% Xredtest=[ones(numel(yrtest),1) Xtest(:,gammat(1:30))];
% w_MLE=(Xred'*Xred)\Xred'*yrtrain;
% Etest=(yrtest-Xredtest*w_MLE)'*(yrtest-Xredtest*w_MLE)/Ntest
% Yhattst=round(mny+(mxy-mny)*(Xredtest*w_MLE));
% Etst=(Year(Ntr+1:end)-Yhattst)'*(Year(Ntr+1:end)-Yhattst)/Ntest;
% sqrt(Etst)
% Eabstst=mean(abs((Year(Ntr+1:end)-Yhattst)))
% % % Check: for how many songs the prediction year is exact
% % % test data
% wrong_pred=sum(Yhattst-Year(Ntr+1:end)~=0)/Ntest*100
% cor_pred=100-wrong_pred