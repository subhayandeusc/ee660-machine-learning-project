%% Feature reduction using l0 regularization (OMP algorithm) -- Subhayan De
clear all;
load YearPredRegData
[X, mu, s] = standardizeCols(X);
[Xtest, mutest, stest]=standardizeCols(Xtest);
%% Orthogonal Matching Pursuit (OMP) (Murphy page 428)

gammat_omp=[];
gammac_omp=1:D;
% w_MLE=[];
 w_MLE=(X(:,gammat_omp)'*X(:,gammat_omp))\X(:,gammat_omp)'*yrtrain;
 maxbeta=zeros(40,1);miner_omp=maxbeta;
for k=1:40
    k
    res=(yrtrain-X(:,gammat_omp)*w_MLE);
    for i=1:numel(gammac_omp)
%         gammat1=[gammat i];
%         w_MLE=(X(:,gammat1)'*X(:,gammat1))\X(:,gammat1)'*yrtrain;
%         Etrain(i)=(yrtrain-X(:,gammat1)*w_MLE)'*(yrtrain-X(:,gammat1)*w_MLE)/Ntr;
        beta(i)=X(:,gammac_omp(i))'*res/norm(X(:,gammac_omp(i)),2)^2;
    end
    [maxbeta(k),imax]=max(beta);
    gammat_omp=[gammat_omp gammac_omp(imax)];
    clear gammac_omp beta
    gammac_omp=[];
    for i=1:D
        counter=1;
        for j=1:numel(gammat_omp)
            if i==gammat_omp(j)
                counter=0;
            end
        end
        if counter~=0
            gammac_omp=[gammac_omp i];
        end
    end
    w_MLE=(X(:,gammat_omp)'*X(:,gammat_omp))\X(:,gammat_omp)'*yrtrain;
    miner_omp(k)=(yrtrain-X(:,gammat_omp)*w_MLE)'*(yrtrain-X(:,gammat_omp)*w_MLE)/Ntr;
end

% save feat_red_40_omp miner_omp gammat_omp gammac_omp
%% Plot the change in training error with increasing number of features
% load feat_red_40_omp
% MSE for training data using all 90 features
w_MLE=(X'*X)\X'*yrtrain;
Etrain=(yrtrain-X*w_MLE)'*(yrtrain-X*w_MLE)/Ntr;
plot(Etrain*ones(numel(miner),1),'color','b','LineWidth',2);
hold on;
plot(miner_omp,'-*','color','magenta','LineWidth',1.5,'MarkerSize',8);
xlim([1 40]);
ylim([0.0115 0.0240]);
xlabel('Number of features ($D$)','interpreter','latex');
ylabel('Mean squared error (MSE)','interpreter','latex');
title('Feature reduction using $l_0$ regularization','interpreter','latex');
ax1 = gca;
hl1=legend({'all $D=90$ features','reduced features using OMP'},'interpreter','latex','fontsize',20);
set(hl1,'color',[0.89 .94 .9]);
ax1.LineWidth=1.0;
ax1.FontName='Times';
ax1.FontSize=20;

%% Plot both SBR and OMP result
figure;
load feat_red_40
load feat_red_40_omp
% MSE for training data using all 90 features
w_MLE=(X'*X)\X'*yrtrain;
Etrain=(yrtrain-X*w_MLE)'*(yrtrain-X*w_MLE)/Ntr;
plot(Etrain*ones(numel(miner),1),'color','b','LineWidth',2);
hold on;
plot(miner,'-o','color','r','LineWidth',1.5,'MarkerSize',8);
plot(miner_omp,'-*','color','magenta','LineWidth',1.5,'MarkerSize',8)
xlim([1 40]);
ylim([0.0115 0.0240]);
xlabel('Number of features ($D$)','interpreter','latex');
ylabel('Mean squared error (MSE)','interpreter','latex');
title('Feature reduction using $l_0$ regularization','interpreter','latex');
ax1 = gca;
hl1=legend({'all $D=90$ features','reduced features using SBR','reduced features using OMP'},'interpreter','latex','fontsize',20);
set(hl1,'color',[0.89 .94 .9]);
ax1.LineWidth=1.0;
ax1.FontName='Times';
ax1.FontSize=20;

